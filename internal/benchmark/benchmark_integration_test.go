package benchmark

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bombsimon/logrusr"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-runner-stress/gitlab-runner-stress/internal/gitlab"
	"gitlab.com/gitlab-runner-stress/gitlab-runner-stress/internal/gitlab/testdata"
)

func TestBenchmark(t *testing.T) {
	benchmarkVar := gitlab.JobVariable{
		Key:   runBenchmarksVarKey,
		Type:  gitlab.EnvironmentVariableType,
		Value: "true",
	}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.String() == "/api/graphql" {
			w.WriteHeader(http.StatusOK)
			fmt.Fprint(w, testdata.ProjectQuerySuccessResp)
			return
		}

		b, err := ioutil.ReadAll(r.Body)
		require.NoError(t, err)

		expectedJSONBody := map[string]interface{}{
			"ref":       "main",
			"variables": []gitlab.JobVariable{benchmarkVar},
		}
		eb, err := json.Marshal(expectedJSONBody)
		require.NoError(t, err)
		require.Equal(t, eb, b)

		w.WriteHeader(http.StatusCreated)
		fmt.Fprintf(w, testdata.PipelineCreateSuccessResp)
	}))
	defer ts.Close()

	app := &cli.App{Writer: ioutil.Discard}
	fSet := flag.NewFlagSet("test", flag.PanicOnError)
	err := fSet.Parse([]string{"benchmark", "--gitlab-instance", ts.URL})
	require.NoError(t, err)

	cliCtx := cli.NewContext(app, fSet, nil)
	cmd := NewCommand(logrusr.NewLogger(logrus.New()))

	err = cmd.Run(cliCtx)
	require.NoError(t, err)
}

func TestBenchmark_APICallsFailed(t *testing.T) {
	tests := map[string]struct {
		endpoint string
	}{
		"project query failed": {
			endpoint: "/api/graphql",
		},
		"pipeline create request failed": {
			endpoint: "/api/v4/projects/123/pipeline?ref=main",
		},
	}

	for testName, tt := range tests {
		t.Run(testName, func(t *testing.T) {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				if tt.endpoint == r.URL.String() {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				if r.URL.String() == "/api/graphql" {
					w.WriteHeader(http.StatusOK)
					fmt.Fprint(w, testdata.ProjectQuerySuccessResp)
				}
			}))
			defer ts.Close()

			app := &cli.App{Writer: ioutil.Discard}
			fSet := flag.NewFlagSet("test", flag.PanicOnError)
			err := fSet.Parse([]string{"benchmark", "--gitlab-instance", ts.URL})
			require.NoError(t, err)
			cliCtx := cli.NewContext(app, fSet, nil)

			cmd := NewCommand(logrusr.NewLogger(logrus.New()))

			err = cmd.Run(cliCtx)
			require.Error(t, err)
		})
	}
}
