package trigger

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-runner-stress/gitlab-runner-stress/internal/gitlab"
)

const triggerProjectPath = "gitlab-org/ci-cd/gitlab-runner-stress/trigger"

type triggerAction struct {
	logger logr.Logger
}

// NewCommand returns a pointer for the cli command that is the point of entry
// of the package.
func NewCommand(logger logr.Logger) *cli.Command {
	trigger := triggerAction{
		logger: logger,
	}

	return &cli.Command{
		Name:   "trigger",
		Action: trigger.action,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "gitlab-instance",
				Usage:    `The base URL of the GitLab instance, for example "https://gitlab.com"`,
				Required: true,
			},
			&cli.IntFlag{
				Name:  "count",
				Usage: "Number of pipelines to create",
				Value: 1,
			},
			&cli.StringFlag{
				Name:  "trigger-project-path",
				Usage: "Path for the trigger project",
				Value: triggerProjectPath,
			},
		},
	}
}

func (t *triggerAction) action(cli *cli.Context) error {
	client := gitlab.NewClient(t.logger, cli.String("gitlab-pat"), cli.String("gitlab-instance"))

	projectID, err := client.ProjectID(cli.Context, cli.String("trigger-project-path"))
	if err != nil {
		return fmt.Errorf("getting project id for %q: %v", cli.String("trigger-project-path"), err)
	}

	for count := 1; count <= cli.Int("count"); count++ {
		url, err := client.CreatePipeline(cli.Context, projectID, nil)
		if err != nil {
			return fmt.Errorf("creating pipeline for project %d: %v", projectID, err)
		}

		t.logger.Info("Created pipeline", "count", count, "url", url)
	}

	return nil
}
