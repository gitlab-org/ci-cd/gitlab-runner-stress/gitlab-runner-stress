package gitlab

import (
	"fmt"
	"testing"

	"github.com/bombsimon/logrusr"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

const testToken = "test-token"

func TestNewClient(t *testing.T) {
	client := NewClient(logrusr.NewLogger(logrus.New()), testToken, "http://example.com")
	require.Equal(t, testToken, client.pat)
}

func TestAPIAuthHeader(t *testing.T) {
	client := NewClient(logrusr.NewLogger(logrus.New()), testToken, "http://example.com")
	headerKey, headerValue := client.graphQLAuthHeader()
	require.Equal(t, "Authorization", headerKey)
	require.Equal(t, fmt.Sprintf("Bearer: %s", testToken), headerValue)

	headerKey, headerValue = client.restAPIAuthHeader()
	require.Equal(t, "PRIVATE-TOKEN", headerKey)
	require.Equal(t, testToken, headerValue)
}
