run:
  # default concurrency is a available CPU number
  concurrency: 4
  # timeout for analysis, e.g. 30s, 5m, default is 1m
  timeout: 1m
  # exit code when at least one issue was found, default is 1
  issues-exit-code: 1
  # include test files or not, default is true
  tests: true

issues:
  exclude-use-default: false
  exclude:
  # should have a package comment, unless it's in another file for this package (golint)
  - 'in another file for this package'

# all available settings of specific linters
linters-settings:
  dogsled:
    # checks assignments with too many blank identifiers; default is 2
    max-blank-identifiers: 2
  dupl:
    # tokens count to trigger issue, 150 by default
    threshold: 100
  errcheck:
    # report about not checking of errors in type assertions: `a := b.(MyStruct)`;
    # default is false: such cases aren't reported by default.
    check-type-assertions: true
    # report about assignment of errors to blank identifier: `num, _ := strconv.Atoi(numStr)`;
    # default is false: such cases aren't reported by default.
    check-blank: false
  gocognit:
    # minimal code complexity to report, 30 by default (but we recommend 10-20)
    min-complexity: 10
  goconst:
    # minimal length of string constant, 3 by default
    min-len: 3
    # minimal occurrences count to trigger, 3 by default
    min-occurrences: 3
  gocritic:
    # Enable multiple checks by tags, run `GL_DEBUG=gocritic golangci-lint run` to see all tags and checks.
    # Empty list by default. See https://github.com/go-critic/go-critic#usage -> section "Tags".
    enabled-tags:
    - performance
    settings: # settings passed to gocritic
      captLocal: # must be valid enabled check name
        paramsOnly: true
      rangeValCopy:
        sizeThreshold: 32
  gocyclo:
    # minimal code complexity to report, 30 by default (but we recommend 10-20)
    min-complexity: 10
  gofmt:
    # simplify code: gofmt with `-s` option, true by default
    simplify: true
  goimports:
    # put imports beginning with prefix after 3rd-party packages;
    # it's a comma-separated list of prefixes
    local-prefixes: gitlab.com/gitlab-runner-stress/gitlab-runner-stress
  golint:
    min-confidence: 0.1
  govet:
    # report about shadowed variables
    check-shadowing: true
    # settings per analyzer
    settings:
      printf: # analyzer name, run `go tool vet help` to see all analyzers
        funcs: # run `go tool vet help printf` to see available settings for `printf` analyzer
        - (github.com/golangci/golangci-lint/pkg/logutils.Log).Infof
        - (github.com/golangci/golangci-lint/pkg/logutils.Log).Warnf
        - (github.com/golangci/golangci-lint/pkg/logutils.Log).Errorf
        - (github.com/golangci/golangci-lint/pkg/logutils.Log).Fatalf
    enable-all: true
  lll:
    # max line length, lines longer will be reported. Default is 120.
    # '\t' is counted as 1 character by default, and can be changed with the tab-width option
    line-length: 120
    # tab width in spaces. Default to 1.
    tab-width: 1
  misspell:
    locale: US
  nakedret:
    # make an issue if func has more lines of code than this setting and it has naked returns; default is 30
    max-func-lines: 1
  unparam:
    # Inspect exported functions, default is false. Set to true if no external program/library imports your code.
    # XXX: if you enable this setting, unparam will report a lot of false-positives in text editors:
    # if it's called for subdir of a project it can't find external interfaces. All text editor integrations
    # with golangci-lint call it on a directory with the changed file.
    check-exported: false
  unused:
    # treat code as a program (not a library) and report unused exported identifiers; default is false.
    # XXX: if you enable this setting, unused will report a lot of false-positives in text editors:
    # if it's called for subdir of a project it can't find funcs usages. All text editor integrations
    # with golangci-lint call it on a directory with the changed file.
    check-exported: false
  whitespace:
    multi-if: false   # Enforces newlines (or comments) after every multi-line if statement
    multi-func: false # Enforces newlines (or comments) after every multi-line function signature
  wsl:
    # If true append is only allowed to be cuddled if appending value is
    # matching variables, fields or types on line above. Default is true.
    strict-append: true
    # Allow calls and assignments to be cuddled as long as the lines have any
    # matching variables, fields or types. Default is true.
    allow-assign-and-call: true
    # Allow multiline assignments to be cuddled. Default is true.
    allow-multiline-assign: true
    # Allow declarations (var) to be cuddled.
    allow-cuddle-declarations: false
    # Allow trailing comments in ending of blocks
    allow-trailing-comment: false
    # Force newlines in end of case at this limit (0 = never).
    force-case-trailing-whitespace: 0
    # Force cuddling of err checks with err var assignment
    force-err-cuddling: false
    # Allow leading comments to be separated with empty liens
    allow-separated-leading-comment: false

linters:
  enable:
  - megacheck
  - govet
  - lll
  - misspell
  - nakedret
  - wsl
  - whitespace
  - bodyclose
  - dogsled
  - goimports
  - gosec
  - golint
  - errcheck
  - staticcheck
  - unused
  - gosimple
  - structcheck
  - varcheck
  - ineffassign
  - deadcode
  - typecheck
  - unparam
  - dupl
  - goconst
  - gocritic
  - gocyclo
  - gofmt
  disable:
  - maligned
  - prealloc
  - scopelint
