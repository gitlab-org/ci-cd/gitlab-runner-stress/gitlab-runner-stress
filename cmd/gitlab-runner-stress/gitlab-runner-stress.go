package main

import (
	"os"

	"github.com/bombsimon/logrusr"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-runner-stress/gitlab-runner-stress/internal/benchmark"
	"gitlab.com/gitlab-runner-stress/gitlab-runner-stress/internal/trigger"
)

func main() {
	logger := logrusr.NewLogger(logrus.New())

	app := &cli.App{
		Name:  "gitlab-runner-stress",
		Usage: "Utility tools to stress a GitLab Runner fleet",
		Commands: []*cli.Command{
			trigger.NewCommand(logger),
			benchmark.NewCommand(logger),
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "gitlab-pat",
				Usage:    "GitLab personal access token: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html",
				EnvVars:  []string{"GITLAB_PAT"},
				Required: true,
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		logger.Error(err, "Command finished with error")
	}
}
